# My Vim Setup 

1. checkout repository
2. Add ~/.vimrc file
```
source <PATHTOREPOSITORY>/.vimrc
```
3. Add ~/.ideavimrc file
```
source <PATHTOREPOSITORY>/.ideavimrc
```
4. Install Vundle  https://github.com/VundleVim/Vundle.vim
A plugin installer and manager
```
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```
install plugins defined in vimrc
```
vim +PluginInstall +qall
```

5. In Intelij install plugins
- ideavim
- kJump



