set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" utils
Plugin 'easymotion/vim-easymotion'
Plugin 'Townk/vim-autoclose'
Plugin 'preservim/nerdtree'
Plugin 'inkarkat/vim-ReplaceWithRegister'
" latex
Plugin 'vim-latex/vim-latex'
Plugin 'ivalkeen/nerdtree-execute'
" Colorscheme
Plugin 'morhetz/gruvbox'

" All of your Plugins must be added before the following line
call vundle#end()            " required

" Set colorscheme
let g:gruvbox_guisp_fallback = "bg"
colorscheme gruvbox
set background=dark

" Enable file type detection and do language-dependent indenting.
filetype plugin indent on    " required

" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ

""""""""""""""""""""
" EASYMOTION
""""""""""""""""""""
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Need two keystrokes, but on average, it may be more comfortable.
nmap s <Plug>(easymotion-overwin-f2)

" Turn on case-insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

""""""""""""""""""""
" vim-latex
""""""""""""""""""""

let g:Tex_DefaultTargetFormat = 'pdf'
" Sets in-line spellchecking
set spell

" Set local language
setlocal spell spelllang=en_us

""""""""""""""""""""
" MAPPINGS
""""""""""""""""""""

" ESC mapping
" Press ii to return to normal mode when in insert mode
inoremap ii <ESC>
inoremap <ESC> <NOP>
" Press ii to return to normal mode when in visual mode
vnoremap ii <ESC>
vnoremap <ESC> <NOP>
" Press ii when in Command mode, to go back to normal mode
cnoremap ii <ESC>

" Ctrl-j/k deletes blank line below/above, and Alt-o/O inserts.
nnoremap <silent><C-j> m`:silent +g/\m^\s*$/d<CR>``:noh<CR>
nnoremap <silent><C-k> m`:silent -g/\m^\s*$/d<CR>``:noh<CR>
nnoremap <A-o> o<ESC>
nnoremap <A-O> O<ESC>

" fix meta-keys which generate <Esc>a .. <Esc>z
for i in range(97,122)
  let c = nr2char(i)
  exec "map \e".c." <M-".c.">"
  exec "map! \e".c." <M-".c.">"
endfor

" Nerd tree mapping
map <C-n> :NERDTreeToggle<CR>

" latex safe and compile
map <f2> :w<cr><leader>ll 

""""""""""""""""""""
" EXTRAS
""""""""""""""""""""

" Toggle spell check
map <F5> :setlocal spell! spelllang=en_us<CR>
" Customize colors for spell check
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=203 guifg=#ff5f5f
hi SpellBad gui=undercurl
hi SpellLocal cterm=underline ctermfg=203 guifg=#ff5f5f
hi SpellRare cterm=underline ctermfg=203 guifg=#ff5f5f
hi SpellCap cterm=underline ctermfg=203 guifg=#ff5f5f


" Make backspace behave in a sane manner.
set backspace=indent,eol,start

" Switch syntax highlighting on
syntax on

" Show line numbers
set relativenumber
set number

" Allow hidden buffers, don't limit to 1 file per window/split
set hidden
" persistent undo
set undofile
set undodir=$HOME/.vim

" Bigger is better
set history=1000
set undolevels=1000
set undoreload=1000

" Search Stuff
" Do not keep search results highlighted
set nohlsearch
" Update search results as you type
set incsearch
" Case insensitive
set ignorecase
" Except if you typed uppercase
set smartcase

""""""""""""""""""""
" Personal functions
""""""""""""""""""""

" xml linter for vim
function! DoPrettyXML()
  silent %!xmllint --format -
endfunction
command! PrettyXML call DoPrettyXML()

